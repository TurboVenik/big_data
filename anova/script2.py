import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt

data = pd.read_csv("dumin.csv", sep=',')

data.plot(kind='kde')
plt.show()

f_val, p_val = stats.f_oneway(data['t0'],data['t1'],data['t2'])
print (f_val, "One-way ANOVA P =", p_val)
