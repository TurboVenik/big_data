import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt

data = pd.read_csv("anova.csv", sep=';', index_col='n')

data.plot(kind='kde')
plt.show()

f_val, p_val = stats.f_oneway(data['t1'],data['t1'])
print (f_val, "One-way ANOVA P =", p_val)



