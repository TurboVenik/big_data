import pandas
import matplotlib.pyplot as plt
from scipy import stats

data = pandas.read_csv('brain_size.csv', sep=';', na_values=".")

groupby_gender = data.groupby('Gender')

female_viq = data[data['Gender'] == 'Female']['Height']
male_viq = data[data['Gender'] == 'Male']['Height']
print(stats.ttest_ind(female_viq, male_viq))

groupby_gender['Height'].plot(kind='kde')
plt.show()